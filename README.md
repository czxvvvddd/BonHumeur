# BonHumeur

### 模板介绍
BonHumeur是设计师Jean设计的一套简洁实用的响应式博客模板，清新大气，基于bootstrap4开发，兼容主流浏览器，已开源，代码规范，可在所有主流web浏览器、平板电脑和手机上无缝运行。非常适合自媒体人、站长等内容输出者使用，如果你想练习建站技术，此模板也是不二之选。目前BonHumeur已集成在开源项目JPress内，能够快速上手，欢迎体验。


### 模板特点

* 采用HTML5和CSS3编写；
* 采用Bootstrap 4编写；
* 响应式布局（台式机、平板电脑、移动设备）
* 对于后台开发，代码友好；
* 清新、现代化设计；
* 支持各类型代码高亮显示；


### 浏览器支持

BonHumeur支持所有现代浏览器（Chrome，Firefox，Safari），包括IE11和Edge。


### 效果展示

![](./screenshots/bonhumeur-index.png)
![](./screenshots/bonhumeur-details.png)


### 联系Jean

* 有关本模板的任何问题，欢迎添加Jean讨论；
* 后续更多开源模板，请添加Jean持续关注；
* 需要sketch文件，Jean很乐意分享；
* 喜欢Jean的风格，期待找Jean定制。
* ![](./screenshots/wechat.jpg)